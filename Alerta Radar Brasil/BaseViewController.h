//
//  BaseViewController.h
//  Movida
//
//  Created by Movida on 28/12/15.
//  Copyright © 2015 Movida. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController<UITextFieldDelegate>

@property(nonatomic, strong) NSArray *textFields;
@property(nonatomic, assign) BOOL desativaAnimacao;
@property(nonatomic) BOOL desativaTap;
@property(nonatomic, strong) UITextField *ultimoTextField;

- (void)addLogoNavigation;
- (void)addBackIcon;
- (void)addBackIconPop;
- (void)addBackIconToRootViewController;

- (void)clickBackPop;
- (void)clickBack;

- (void)backToRootViewController;

- (void)initTap;

- (void)dismissKeyboard;

- (void)configdismissOnTap:(NSArray *)textFields;

- (void)setTitleNavigation:(NSString *)title;

@end
