//
//  HomeViewController.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "HomeViewController.h"
#import <GoogleMobileAds/GoogleMobileAds.h>
#import "SharedPreferences.h"
#import "MenuViewController.h"
#import "UIColor+Extras.h"
#import <MZFormSheetPresentationViewController.h>
#import <AudioToolbox/AudioToolbox.h>


@interface HomeViewController ()<MenuViewControllerDelegate>
{
    
    __weak IBOutlet GADBannerView *bvBottom;
    __weak IBOutlet UIView *viewVelocidade;
    GADRequest *request;
    GADRequest *requestBanner;
    float ultimaDistancia;
    __weak IBOutlet UILabel *lbKM;
    __weak IBOutlet UILabel *lbDistancia;
    __weak IBOutlet UILabel *lbTipo;
    __weak IBOutlet UIButton *btAtivar;
    __weak IBOutlet UILabel *lbVelocidade;
    Boolean iniciado;
    
     SystemSoundID mySound;
}

@property(nonatomic, strong) GADInterstitial *interstitial;


@end

@implementation HomeViewController


-(void)viewWillAppear:(BOOL)animated{
    
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(atualizaLocalizacao) name:@"AtualizaLocalizacao" object:nil];
}



- (void)playSound :(Radar *)radar {
  
    if([radar.tipo isEqualToString:@"Policia Rodoviaria"]
       || [radar.tipo isEqualToString:@"Pedagio"]
       || [radar.tipo isEqualToString:@"Lombada"])
        return;
    
    
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"alerta" ofType:@"mp3"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL,
                                     &mySound);
    AudioServicesPlaySystemSound(mySound);
}

- (void)viewDidLoad {
    
    
    
    
    [super viewDidLoad];
    
    
    
    
    self.naoDesativaUpdate = YES;
    
    viewVelocidade.layer.borderColor = [UIColor redColor].CGColor;
    viewVelocidade.layer.borderWidth = 1.0f;
    
    
    request = [GADRequest request];

    
    bvBottom.adUnitID = NSLocalizedString(@"ADMobKeyBanner", nil);
    bvBottom.rootViewController = self;
    
    
    
    [SharedPreferences setDistancia:@700];
    
    if(![SharedPreferences isInstalled]){
        
        [self presentViewController:
         [self.storyboard instantiateViewControllerWithIdentifier:@"InstallViewController"] animated:YES completion:nil];
    }
//
    
    
    [self createAndLoadInterstitial];
    
    [self performSelector:@selector(showBanner) withObject:nil afterDelay:5];
    
    [self createBanner];
    
    
    
}


-(void)atualizaLocalizacao{
    
    
       if(!iniciado)
        return;
    
   
    CCSQLite *SQlite =  [UIHelper getDatabase:NO];
    


    Radar *radar;
    
      if([SQlite open]){
          
          NSString *query = [NSString stringWithFormat:@"SELECT * FROM Radar order by abs(latitude - %f) + abs(longitude - %f) limit 1",self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude];
          
          CCResultSet *result = [SQlite executeQuery:query];
          
          
          while ([result next]) {
              
              radar = [Radar new];
              radar.idRadar = @([result intForColumnIndex:0]);
              radar.latitude = [result doubleForColumnIndex:1];
              radar.longitude = [result doubleForColumnIndex:2];
              radar.velocidade = [result stringForColumnIndex:3];
              radar.tipo = [result stringForColumnIndex:4];
              
          }
          
      }

    
    [SQlite close];
    
    
    int velocidade =[self calculaVelocidade];
    
    lbVelocidade.text =[NSString stringWithFormat:@"Velocidade atual: %d km/h",velocidade];
    
    float distancia =0;
    
    if(radar){
        
        
        distancia = [radar distanceTo:self.currentLocation];
        
        
        if (distancia < [[SharedPreferences distancia] intValue]) {
        
        
            if(ultimaDistancia == 0 || ultimaDistancia >= distancia){
                
                lbKM.text = [NSString stringWithFormat:@"%@km/h",radar.velocidade];
                lbDistancia.text = [NSString stringWithFormat:@"À %d metros de distância", (int)distancia];
                lbTipo.text = radar.tipo;
                
                
                
                if([radar.velocidade intValue] < velocidade)
                {
                    
                    //todo start alert
                    
                    if([SharedPreferences alerta])
                        [self playSound:radar];
                    else
                        AudioServicesRemoveSystemSoundCompletion(mySound);
                    
                    
                    lbKM.tintColor = [UIColor redColor];

                }
                else
                    AudioServicesRemoveSystemSoundCompletion(mySound);
                
            }
            else{
                
                lbKM.tintColor = [UIColor blackColor];
                lbKM.tintColor = [UIColor blackColor];
                lbKM.text = @"-";
                lbTipo.text = @"-";
                lbDistancia.text = @"-";

                AudioServicesRemoveSystemSoundCompletion(mySound);
            }
            
        }
        else{
            
            lbKM.tintColor = [UIColor blackColor];
            lbKM.text = @"-";
            lbTipo.text = @"-";
            lbDistancia.text = @"-";
            AudioServicesRemoveSystemSoundCompletion(mySound);

        }
        
        ultimaDistancia = distancia;
            
        
    }
    else{
        
        lbKM.tintColor = [UIColor blackColor];
        lbKM.text = @"-";
        lbTipo.text = @"-";
        lbDistancia.text = @"-";
        AudioServicesRemoveSystemSoundCompletion(mySound);
    }
    
}

-(int)calculaVelocidade{
    
    int velocidade = (int)self.currentLocation.speed * 3600 /1000;
    
    return velocidade < 0 ? 0 :velocidade;
    
}



- (void)createAndLoadInterstitial {
    self.interstitial =
    [[GADInterstitial alloc] initWithAdUnitID:NSLocalizedString(@"ADMobKeyTelaCheia", nil)];
    request = [GADRequest request];
    [self.interstitial loadRequest:request];
    [self performSelector:@selector(showBanner) withObject:nil afterDelay:60*10];

}

-(void)createBanner{
    
    requestBanner = [GADRequest request];
    [bvBottom loadRequest:requestBanner];
    
    
    [self performSelector:@selector(createBanner) withObject:nil afterDelay:60*1];
}


-(void)showBanner{
    
    if (self.interstitial.isReady) {
        [self.interstitial presentFromRootViewController:self];
        
    }
    
    [self createAndLoadInterstitial];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onMenuTap:(id)sender {
    
    
    MenuViewController *menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    
    menuVC.delegate = self;
    
    [self presentViewController:menuVC animated:YES completion:nil];
}


-(void)menuPreferenciasTaped{
    
    [self performSegueWithIdentifier:@"preferenciasSegue" sender:nil];
    
}

-(void)menuRadaresTaped{
    
    [self performSegueWithIdentifier:@"radares" sender:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AtualizaLocalizacao" object:nil];

    [super stopUpdate];
}
- (IBAction)onAtivarTap:(id)sender {
    
    if(iniciado){
        
        iniciado = NO;
        
        [btAtivar setTitle:@"Ativar Monitoramento" forState:UIControlStateNormal];
        [btAtivar setBackgroundColor:[UIColor colorWithHexString:@"#408000"]];
        [self stopUpdate];
    }
    else{
        
        iniciado = YES;
        [btAtivar setBackgroundColor:[UIColor redColor]];
        [btAtivar setTitle:@"Parar Monitoramento" forState:UIControlStateNormal];
        
        [self startUpdate];
    }
    
}

- (IBAction)onCombustivelTap:(id)sender {
 
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AbastecerViewController"];
    
    
    MZFormSheetPresentationViewController* form =[[MZFormSheetPresentationViewController alloc]initWithContentViewController:vc];

    
    

    form.contentViewControllerTransitionStyle = MZFormSheetPresentationTransitionStyleSlideFromTop;
    
    form.presentationController.shouldApplyBackgroundBlurEffect = YES;
    form.presentationController.shouldDismissOnBackgroundViewTap = YES;
    form.presentationController.blurEffectStyle = UIBlurEffectStyleDark;
    form.presentationController.shouldCenterVertically = YES;
    form.presentationController.shouldCenterHorizontally = YES;
    form.presentationController.movementActionWhenKeyboardAppears = MZFormSheetActionWhenKeyboardAppearsAboveKeyboard;
    
    
    [self presentViewController:form animated:YES completion:nil];
    
    
    
}


@end
