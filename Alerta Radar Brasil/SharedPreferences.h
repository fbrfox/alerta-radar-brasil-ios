//
//  SharedPreferences.h
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/04/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedPreferences : NSObject

+(NSString *)isInstalled;
+(void)setInstalled:(NSString *)installed;

+(NSNumber *)distancia;

+(void)setDistancia:(NSNumber *)distancia;

+(void)setAlerta:(NSNumber *)alerta;
+(BOOL)alerta;


@end
