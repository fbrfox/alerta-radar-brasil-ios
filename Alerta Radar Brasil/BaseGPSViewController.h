//
//  BaseGPSViewController.h
//  Movida
//
//  Created by Movida on 04/01/16.
//  Copyright © 2016 Movida. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreLocation;

@interface BaseGPSViewController : BaseViewController

@property(nonatomic, strong) CLLocation *currentLocation;
@property(nonatomic, readwrite) BOOL naoDesativaUpdate;

- (BOOL)verificaStatusGPS;
- (void)startUpdate;
-(void)stopUpdate;

@end
