//
//  MenuViewController.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onFecharMenuTap:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onRadaresTap:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate menuRadaresTaped];
}

- (IBAction)onPreferenciasTap:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.delegate menuPreferenciasTaped];
}

- (IBAction)onCompartilharTap:(id)sender {
    NSArray *itens = [[NSArray alloc]initWithObjects:@"https://itunes.apple.com/us/app/alerta-radara-brasil/id1266014761?l=pt&ls=1&mt=8", nil];
    
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:itens applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];

    
}
- (IBAction)onAvaliarTap:(id)sender {

    [self dismissViewControllerAnimated:YES completion:^{

        [UIHelper openUrl:@"https://itunes.apple.com/us/app/alerta-radara-brasil/id1266014761?l=pt&ls=1&mt=8"];
    }];
}


@end
