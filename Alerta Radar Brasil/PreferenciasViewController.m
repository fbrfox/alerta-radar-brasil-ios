//
//  PreferenciasViewController.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "PreferenciasViewController.h"

@interface PreferenciasViewController ()
{
    __weak IBOutlet UISwitch *swAlerta;
    
}
@end

@implementation PreferenciasViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [swAlerta setOn: [SharedPreferences alerta]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onSalvarTap:(id)sender {
    
    
    [SharedPreferences setAlerta:@(swAlerta.isOn)];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
