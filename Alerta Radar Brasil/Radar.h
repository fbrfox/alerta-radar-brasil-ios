//
//  Radar.h
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//


@interface Radar : NSObject

@property(nonatomic,strong) NSNumber *idRadar;

@property(nonatomic,assign) float latitude;

@property(nonatomic,assign) float longitude;

@property(nonatomic,strong) NSString *velocidade;

@property(nonatomic,strong) NSString *tipo;

@property(nonatomic,strong) NSString *descricao;


-(float)distanceTo:(CLLocation *)location;

@end
