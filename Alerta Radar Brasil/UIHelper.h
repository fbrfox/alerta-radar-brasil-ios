//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CCSQLite.h"



@protocol UIHelperDelegate <NSObject>

@optional

-(void)alertControllerOkClicked;

@end

@interface UIHelper : NSObject





//+ (UIHelper *)sharedInstance;

+ (void)mostrarAlerta:(NSString *)mensagem  viewController:(UIViewController *)vc;

+ (void)mostrarAlertaErro:(NSString *)mensagem  viewController:(UIViewController *)vc;

+ (void)mostrarAlertaInfo:(NSString *)mensagem  viewController:(UIViewController *)vc;

+ (void)mostrarAlertaSucesso:(NSString *)mensagem viewController:(UIViewController *)vc;

+ (void)mostrarAlertaSucesso:(NSString *)mensagem delegate:(id<UIHelperDelegate>)delegate  viewController:(UIViewController *)vc;


+ (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem delegate:(id<UIHelperDelegate>)delegate viewController:(UIViewController *)vc;

+ (NSString *)formatarPreco:(NSNumber *)number;


+ (void)showProgress;

+ (void)showProgresswithText:(NSString *)text;

+ (void)configuraNumberPadComDoneButton:(UITextField *)textField target:(id)target action:(SEL)action;


+ (NSString *)randomStringWithLength:(int)len;


+ (NSString *)dataParaString:(NSDate *)date formato:(NSString *)formato;

+ (void)hideProgress;

+ (UIImage *)obterImagemPorCor:(UIColor *)color;

+ (UIColor *)colorFromHexString:(NSString *)hexString;

+ (void)htmlInLabel:(UILabel *)label html:(NSString *)html;

+(NSString *)adicionaDias:(int)dias data:(NSDate *)data formato:(NSString *)formato;

+ (NSDate *)stringParaData:(NSString *)date formato:(NSString *)formato;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

+(NSMutableString*) timeLeftSinceDate: (NSDate *) dateT;

+(NSString *)imageToNSString:(UIImage *)image;
+(NSData *)imageToData:(UIImage *)image;


+(NSDate *)adicionaMinutos:(int)minutos data:(NSDate *)data;

+(BOOL)isConnected;



+(NSString *)imageUrlToBase64Image:(NSString *)imageUrl;

+(void)callNumber:(NSString *)tel;
+(void)sendMail:(NSString *)email;

+(void)openUrl:(NSString *)url;

+(CCSQLite *)getDatabase:(BOOL)create;

@end
