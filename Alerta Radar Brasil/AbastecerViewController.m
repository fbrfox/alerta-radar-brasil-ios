//
//  AbastecerViewController.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "AbastecerViewController.h"

@interface AbastecerViewController ()<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *tfGasolina;
@property (weak, nonatomic) IBOutlet UITextField *tfAlcool;

@end

@implementation AbastecerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    
    if(_tfGasolina.text.length == 4 && ![string isEqualToString:@""])
        return NO;
    
    if(_tfGasolina.text.length == 0)
    {
        _tfAlcool.text = @"";
        return YES;
        
    }
    
    
    float valor = [[_tfGasolina.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    
    float valorAlcool = valor * 0.7f;
    
    _tfAlcool.text = [NSString stringWithFormat:@"%.2f",valorAlcool];
    
    return YES;
    
}
- (IBAction)onFecharTap:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
