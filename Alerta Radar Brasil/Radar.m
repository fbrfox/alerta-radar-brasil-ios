//
//  Radar.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "Radar.h"

@implementation Radar

-(float)distanceTo:(CLLocation *)location{
    
    CLLocation *locationRadar =[[CLLocation alloc]initWithLatitude:self.latitude  longitude:self.longitude];
    return [locationRadar distanceFromLocation:location];
}

@end
