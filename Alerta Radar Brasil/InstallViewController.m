//
//  InstallViewController.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "InstallViewController.h"
#import <CHCSVParser.h>
#import "CCSQLite.h"



@interface InstallViewController ()<CHCSVParserDelegate>
{
    __weak IBOutlet UILabel *lbPorcentagem;
    __weak IBOutlet UIProgressView *pvPorcentagem;
    float totalLinhas;
    float totalInseridas; ;
 
    NSMutableArray  *currentRow;
    NSMutableDictionary *dict;
}
@end

@implementation InstallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
     totalLinhas = 33942;
     totalInseridas = 0;
    
    
    [UIHelper showProgresswithText:@"Aguarde estamos preparando a base de dados"];
   
    [self performSelectorInBackground:@selector(performInstall) withObject:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)performInstall{
    
    BOOL result;
    
    
    
    NSArray *latestFileComponentsArray = [NSArray arrayWithContentsOfCSVURL:[[NSBundle mainBundle]
                                                                             URLForResource: @"maparadar" withExtension:@"csv"]];
    
    
    [UIHelper hideProgress];
    

    
    CCSQLite * SQLite = [UIHelper getDatabase:YES];
    
    
    
     result = [SQLite executeUpdate: @"DROP TABLE IF EXISTS Radar; Drop Index IX_LAT_LONG;"];
    
    if ([SQLite open]) {
        result = [SQLite executeUpdate: @"CREATE TABLE IF NOT EXISTS Radar (Id INTEGER PRIMARY KEY AUTOINCREMENT, Latitude Float, Longitude Float, Velocidade VARCHAR(100) NOT NULL, Tipo VARCHAR(100) NOT NULL); Create Index IX_LAT_LONG on Radar (Latitude,Longitude);"];
        if (result) {
            NSLog(@"create table alerta ok");
        }
    }
    
    
    
    
    for (NSArray *line in latestFileComponentsArray) {
        
        
        Radar *radar = [Radar new];
        
        radar.longitude = [line[0] floatValue];
        radar.latitude =[line[1] floatValue];
        
        NSString *descricao = line[2];
        
        NSArray *descricaoFields =  [[descricao stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] componentsSeparatedByString:@"-"];
        
        
        radar.tipo = descricaoFields[0];
        
        NSArray *velocidadeFields = [descricaoFields[1] componentsSeparatedByString:@"@"];
        
        radar.velocidade = velocidadeFields[1];
        
        
        if([SQLite open]){
            NSString *sql = [NSString stringWithFormat:@"Insert Into Radar (Latitude,Longitude,Velocidade,Tipo) Values('%f','%f','%@','%@');",radar.latitude,radar.longitude,radar.velocidade,radar.tipo];
            
            result = [SQLite executeUpdate:sql];
        }
       
        
        
        
        
        totalInseridas++;
        
        
        if(result)
            NSLog(@"Linha inserida %f",totalInseridas);
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            float percentual = totalInseridas/totalLinhas *100;

            
            pvPorcentagem.progress = percentual / 100;
            lbPorcentagem.text = [NSString stringWithFormat:@"%d%%",(int)percentual];
            
        });
        
        
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:^{
            
            [SharedPreferences setInstalled:@"Sim"];;
        }];
    });
    
    
    
    
    
    

    
}

-(void) parserDidBeginDocument:(CHCSVParser *)parser
{
    currentRow = [[NSMutableArray alloc] init];
}

-(void) parserDidEndDocument:(CHCSVParser *)parser
{
    for(int i=0;i<[currentRow count];i++)
    {
        NSLog(@"%@          %@          %@",[[currentRow objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"0"]],[[currentRow objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"1"]],[[currentRow objectAtIndex:i] valueForKey:[NSString stringWithFormat:@"2"]]);
    }
}

- (void) parser:(CHCSVParser *)parser didFailWithError:(NSError *)error
{
    NSLog(@"Parser failed with error: %@ %@", [error localizedDescription], [error userInfo]);
}

-(void)parser:(CHCSVParser *)parser didBeginLine:(NSUInteger)recordNumber
{
    dict=[[NSMutableDictionary alloc]init];
}

-(void)parser:(CHCSVParser *)parser didReadField:(NSString *)field atIndex:(NSInteger)fieldIndex
{
    [dict setObject:field forKey:[NSString stringWithFormat:@"%d",fieldIndex]];
}

- (void) parser:(CHCSVParser *)parser didEndLine:(NSUInteger)lineNumber
{
    [currentRow addObject:dict];
    dict=nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
