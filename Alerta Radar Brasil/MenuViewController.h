//
//  MenuViewController.h
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuViewControllerDelegate<NSObject>

-(void)menuPreferenciasTaped;
-(void)menuRadaresTaped;

@end

@interface MenuViewController : UIViewController

@property(nonatomic,weak) id<MenuViewControllerDelegate> delegate;

@end
