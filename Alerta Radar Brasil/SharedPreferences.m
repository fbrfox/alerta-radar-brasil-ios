//
//  SharedPreferences.m
//  Movida Condominio
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 17/04/17.
//  Copyright © 2017 Movida Locação de Veículos Ltda. All rights reserved.
//

#import "SharedPreferences.h"

@implementation SharedPreferences


+(void)setInstalled:(NSString *)installed{
    
    [[NSUserDefaults standardUserDefaults] setObject:installed forKey:@"Installed"];
}


+(NSString *)isInstalled{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Installed"];
}
+(void)setDistancia:(NSNumber *)distancia{
    
    [[NSUserDefaults standardUserDefaults] setObject:distancia forKey:@"Distancia"];
    
}

+(NSNumber *)distancia{
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Distancia"];
}


+(void)setAlerta:(NSNumber *)alerta{
    
    [[NSUserDefaults standardUserDefaults] setObject:alerta forKey:@"Alerta"];
    
}

+(BOOL)alerta{
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"Alerta"];
}
@end
