//
//  BaseViewController.m
//  Movida
//
//  Created by Movida on 28/12/15.
//  Copyright © 2015 Movida. All rights reserved.
//

#import "BaseViewController.h"


@interface BaseViewController ()
@end

@implementation BaseViewController{
    IBOutlet NSLayoutConstraint *topConstraint;
    CGFloat originalConstraint;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addLogoNavigation];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    if (!self.desativaTap) {
        [self initTap];
    }

    originalConstraint = topConstraint.constant;
 
}




#pragma mark - Properties

-(void)setTitleNavigation:(NSString *)title {

    @try {
        self.title = title;
        
        if (self.navigationItem) {
            self.navigationItem.title = title;
        } else if (self.navigationController.navigationItem) {
            self.navigationController.navigationItem.title = title;
        } else if (self.parentViewController.navigationController.navigationItem) {
            self.parentViewController.navigationController.navigationItem.title = title;
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception occurred: %@, %@", exception, [exception userInfo]);
    }
}

- (void)addLogoNavigation {

//    UIImageView *imageLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_transparente.png"]];
//    
//    imageLogo.frame = CGRectMake(imageLogo.frame.origin.x, imageLogo.frame.origin.y, 90, 40);
//    imageLogo.contentMode = UIViewContentModeScaleAspectFit;
//    
//    self.navigationItem.titleView = imageLogo;
}

- (void)addBackIcon {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back"] style:UIBarButtonItemStylePlain target:self action:@selector(clickBack)];
}

- (void)addBackIconPop {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(clickBackPop)];
}

- (void)clickBackPop {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)clickBack {

    @try {
        [self.navigationController popViewControllerAnimated:YES];
        [self.parentViewController.navigationController popViewControllerAnimated:YES];
    } @catch (NSException *exception) {
        NSLog(@"Exception occurred: %@, %@", exception, [exception userInfo]);
    }
}

- (void)backToRootViewController {

    @try {
        if (self.navigationController) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
        if (self.parentViewController.navigationController) {
            [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
        }
    } @catch (NSException *exception) {
        NSLog(@"Exception occurred: %@, %@", exception, [exception userInfo]);
    }
}

- (void)addBackIconToRootViewController{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backToRootViewController)];
}

- (void)initTap {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];

    [tap setCancelsTouchesInView:NO];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {

    self.ultimoTextField = textField;
    
    if (_desativaAnimacao) {
        return;
    }

    CGRect textFieldRect = [self.view.window convertRect:textField.bounds fromView:textField];
    CGFloat midline = (CGFloat) (textFieldRect.origin.y + 0.5 * textFieldRect.size.height);

    topConstraint.constant = -(midline  - textFieldRect.size.height) + 120;

    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {

    topConstraint.constant = originalConstraint;

    [UIView animateWithDuration:0.3f animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)dismissKeyboard {
    
    for (UIView *txt in self.textFields) {
        [txt resignFirstResponder];
    }

    [self.view endEditing:YES];
}

- (void)configdismissOnTap:(NSArray *)textFields {

    self.textFields = textFields;

    for (UIView *textField in textFields) {
        if ([textField isKindOfClass:[UITextField class]]) {
            ((UITextField *)textField).delegate = self;
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {

    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
    self.textFields = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self dismissKeyboard];
}

- (IBAction)onTelefonarTap:(id)sender {
    NSURL *phoneURL = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@",@"08006068686"]];
    [[UIApplication sharedApplication] openURL:phoneURL options:nil completionHandler:nil];
}
- (IBAction)onBackClick:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBackPopClick:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
