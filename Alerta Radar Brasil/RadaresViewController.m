//
//  RadaresViewController.m
//  Alerta Radar Brasil
//
//  Created by Pedro Henrique  Borges de Paula Sebastiao on 31/07/17.
//  Copyright © 2017 ph2p. All rights reserved.
//

#import "RadaresViewController.h"
@import GoogleMaps;

@interface RadaresViewController ()<GMSMapViewDelegate>
{
    
    __weak IBOutlet GMSMapView *mapaView;
}
@end



@implementation RadaresViewController

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    mapaView.delegate = self;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addPosition) name:@"AtualizaLocalizacao" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addPosition{
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.currentLocation.coordinate.latitude longitude:self.currentLocation.coordinate.longitude zoom:15];
    
    [mapaView animateToCameraPosition:camera];
    
    [super stopUpdate];
    
    
    [self apresentaRadares:mapaView location:self.currentLocation.coordinate];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    if(gesture)
        [self apresentaRadares:mapView];
}

- (void)apresentaRadares:(GMSMapView *)mapView location:(CLLocationCoordinate2D)location {
  CCSQLite *SQlite =  [UIHelper getDatabase:NO];
    
    if([SQlite open]){
        
        NSString *query = [NSString stringWithFormat:@"SELECT * FROM Radar order by abs(latitude - %f) + abs(longitude - %f) limit 50",location.latitude,location.longitude];
        
        CCResultSet *result = [SQlite executeQuery:query];
        
        
        while ([result next]) {
            
            Radar* radar = [Radar new];
            radar.idRadar = @([result intForColumnIndex:0]);
            radar.latitude = [result doubleForColumnIndex:1];
            radar.longitude = [result doubleForColumnIndex:2];
            radar.velocidade = [result stringForColumnIndex:3];
            radar.tipo = [result stringForColumnIndex:4];
            
            
            GMSMarker *startMarker = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(radar.latitude, radar.longitude)];
            
            startMarker.map = mapView;
            startMarker.title = [NSString stringWithFormat:@"%@ - %@",radar.tipo, radar.velocidade];

            
        }
        
    }
    
    
    [SQlite close];
}

-(void)apresentaRadares:(GMSMapView *)mapView{
    
    [mapView clear];
    
    CLLocationCoordinate2D location = mapView.camera.target;
    
    [self apresentaRadares:mapView location:location];
    
    
   
    
}

@end
