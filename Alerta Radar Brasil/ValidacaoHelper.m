//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import "ValidacaoHelper.h"


@implementation ValidacaoHelper

+ (ValidacaoHelper *)sharedInstance {

    static ValidacaoHelper *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[ValidacaoHelper alloc] init];
    });

    return _sharedManager;
}

- (NSString *)validarStringNull:(NSString *)string {

    if ([string isEqual:[NSNull null]] || !string) {

        return @"";
    }

    return string;
}


- (BOOL)validarString:(NSString *)string {

    return string && ![string isEqualToString:@""];

}

- (BOOL)senhaConfere:(NSString *)senha comConfirmacaoSenha:(NSString *)confirmacaoSenha {

    if ([senha isEqualToString:confirmacaoSenha])
        return YES;

    return NO;

}

- (BOOL)validarCPF:(NSString *)cpf {

    cpf = [self removerCaracteres:cpf];

    NSUInteger i, firstSum, secondSum, firstDigit, secondDigit, firstDigitCheck, secondDigitCheck;
    if (cpf == nil) return NO;

    if ([cpf length] != 11) return NO;
    if (([cpf isEqual:@"00000000000"]) || ([cpf isEqual:@"11111111111"]) || ([cpf isEqual:@"22222222222"]) || ([cpf isEqual:@"33333333333"]) || ([cpf isEqual:@"44444444444"]) || ([cpf isEqual:@"55555555555"]) || ([cpf isEqual:@"66666666666"]) || ([cpf isEqual:@"77777777777"]) || ([cpf isEqual:@"88888888888"]) || ([cpf isEqual:@"99999999999"])) return NO;

    firstSum = 0;
    for (i = 0; i <= 8; i++) {
        firstSum += [[cpf substringWithRange:NSMakeRange(i, 1)] intValue] * (10 - i);
    }

    if (firstSum % 11 < 2)
        firstDigit = 0;
    else
        firstDigit = 11 - (firstSum % 11);

    secondSum = 0;
    for (i = 0; i <= 9; i++) {
        secondSum = secondSum + [[cpf substringWithRange:NSMakeRange(i, 1)] intValue] * (11 - i);
    }

    if (secondSum % 11 < 2)
        secondDigit = 0;
    else
        secondDigit = 11 - (secondSum % 11);

    firstDigitCheck = [[cpf substringWithRange:NSMakeRange(9, 1)] intValue];
    secondDigitCheck = [[cpf substringWithRange:NSMakeRange(10, 1)] intValue];

    if ((firstDigit == firstDigitCheck) && (secondDigit == secondDigitCheck))
        return YES;
    return NO;
}

- (BOOL)validarEmail:(NSString *)checkString {
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)validarTelefone:(NSString *)telefone {

    NSString *phoneRegex = @"^[0-9]{9}[0-9]{5}-[0-9]{4}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:telefone];
}


- (NSString *)removerCaracteres:(NSString *)string {

    NSCharacterSet *notAllowedChars = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"] invertedSet];
    NSString *resultString = [[string componentsSeparatedByCharactersInSet:notAllowedChars] componentsJoinedByString:@""];
    return [resultString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}


@end
